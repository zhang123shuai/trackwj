import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// 人员画像
import portrait from '../views/ryhx/index.vue'
// 知识图谱
import graph from '../views/zstp/index.vue'
import wbzsgl from '../views/zstp/wbzsgl.vue'
// 态势信息
import situation from '../views/taishi/index.vue'

const routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function (location) {
  return routerPush.call(this, location).catch(err => { })
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/portrait',
    children: [
      {
        path: '/portrait',
        name: 'portrait',
        component: portrait,
        meta: {
          title: '人员画像'
        },
      },
      {
        path: '/graph',
        name: 'graph',
        component: graph,
        meta: {
          title: '知识图谱'
        },
      },
      {
        path: '/wbzsgl',
        name: 'wbzsgl',
        component: wbzsgl,
        meta: {
          title: '知识图谱'
        },
      }

    ],
  },
  {
    path: '/situation',
    name: 'situation',
    component: situation,
  },
]
const router = new VueRouter({
  routes
})

router.beforeEach((to, form, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  } else {
    document.title = 'Wj' //此处写默认的title
  }
  next()
})

export default router
