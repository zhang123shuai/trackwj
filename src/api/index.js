import axios from '@/lib/axios'
import qs from 'qs'
// const access_token = sessionStorage.getItem('access_token');

// // 平台access_token
// export function tokeninfo(data) {
//     return axios({
//         method: 'post',
//         // 可在此添加请求头
//         headers: {
//             'Content-Type': 'application/x-www-form-urlencoded',
//             'Authorization': access_token,
//         },
//         url: `${ipConfigPingTai}/sso/oauth2/token`,
//         data:qs.stringify(data)
//     });
// }

// 根据code获取access_token
export function oauth2token(data) {
    return axios({
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Basic ${encode('clientId:clientSecret')}`
        },
        url: `${ipConfigPingTai}/sso/oauth2/token`,
        data: qs.stringify(data)
    });
}

// 字符串转base64
function encode(str) {
    // 对字符串进行编码
    var encode = encodeURI(str);
    // 对编码的字符串转化base64
    var base64 = btoa(encode);
    return base64;
}
//监控告警
export function ApiDataResourcealarm_search_all(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/alarm_search_all`,
        data
    });
}
//事件脉络
export function ApiDataResourceevent_search_all(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/event_search_all`,
        data
    });
}
//人员档案
export function ApiDataResourceperson_searchByPhone(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/person_searchByPhone`,
        data
    });
}
//通联次数最多/关系密切
export function ApiDataResourcerelation_searchByPhoneCnt(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/relation_searchByPhoneCnt`,
        data
    });
}
//最近一次联系人
export function ApiDataResourcerelation_searchByPhoneTime(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/relation_searchByPhoneTime`,
        data
    });
}
//通联图谱
export function ApiDataResourcerelation_searchAllByPhone(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/relation_searchAllByPhone`,
        data
    });
}
//关联分析人（没用此接口）
export function ApiDataResourcepersonLocation_searchAll(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/personLocation_searchAll`,
        data
    });
}
//群体态势
export function ApiDataResourcegroup_searchAll(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/group_searchAll`,
        data
    });
}
//群体态势
export function ApiDataResourcegroup_searchById(data) {
    return axios({
        method: 'post',
        url: `${configApi}/daas/api/daasDMS/ApiDataResource/group_searchById`,
        data
    });
}